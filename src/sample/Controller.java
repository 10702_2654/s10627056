package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {
    public Button btnone;
    public Button btntwo;
    public Button btnthree;
    public Button btnfour;
    public Button btnfive;
    public Button btnsix;
    public Button btnseven;
    public Button btneigth;
    public Button btnnine;
    public Button btnzero;
    public Label monitor;
    public Button btnplus;
    public Button btnless;
    public Button btnmultiply;
    public Button btnexcept;
    public Button btndet;
    public Button btnanswer;
    public double a=0,b=0;
    String x;
    public void doclick(ActionEvent actionEvent) {
        Button button  = (Button) actionEvent.getSource();
        String number = monitor.getText();
        monitor.setText(number.concat(button.getText()));
    }

    public void docom(ActionEvent actionEvent) {
        Button button  = (Button) actionEvent.getSource();
        switch(button.getText()){
            case "+":
                x="+";
                a=Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "-":
                x="-";
                a=Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "*":
                x="*";
                a=Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "/":
                x="/";
                a=Integer.parseInt(monitor.getText());
                monitor.setText("");
                break;
            case "=":
                if(x=="+")
                { b=Integer.parseInt(monitor.getText());
                    monitor.setText(String.valueOf((int)a+b));}
                if(x=="-")
                { b=Integer.parseInt(monitor.getText());
                    monitor.setText(String.valueOf((int)a-b));}
                if(x=="*")
                { b=Integer.parseInt(monitor.getText());
                    monitor.setText(String.valueOf((int)a*b));}
                if(x=="/")
                { b=Integer.parseInt(monitor.getText());
                    monitor.setText(String.valueOf((int)a/b));}
                break;
        }
    }
}
